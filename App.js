import React, { Component } from "react";
import {
  StyleSheet, Text, View, Switch,
  ImageBackground, ToastAndroid, TouchableOpacity, Animated, Easing,
  TouchableWithoutFeedback

} from "react-native";
import axios from "axios";
import { Icon } from 'react-native-elements';
import { setCustomText } from 'react-native-global-props';
import LottieView from 'lottie-react-native';

// Setting default styles for all Text components.
const customTextProps = {
  style: {
    fontFamily: "iransans"
  }
};

setCustomText(customTextProps);

type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      isAlarmOn: false,
      isLocal: true
    };
  }



  onOrOff = function (isOn) {

    let parameter = isOn ? "1" : "0";
    let baseUrl = this.state.isLocal ? "http://192.168.1.150/url?switch=" : "http://behzadshirani.ir";

    this.setState({ isAlarmOn: isOn });
    axios
      .get(baseUrl + parameter)
      .then(response => {
        this.setState({ isAlarmOn: isOn });
      })
      .catch(error => {
        this.setState({ isAlarmOn: !isOn });
        ToastAndroid.show(
          "ارتباط با شبکه برقرار نیست",
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM
        );
      });
  };

  render() {
    return (
      <ImageBackground
        style={styles.container}
        source={require("./images/background.jpg")}
      >
        <View style={styles.header}>
          <Text style={styles.headerTitle}> کنترل دزدگیر بهسا </Text>
        </View>

        <View style={styles.contentContainer}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-around"
            }}
          >

            <Icon
              type="font-awesome"
              name="lock"
              color="#EC1661"
              reverse
              raised
              component={TouchableOpacity}
              size={30}
              onPress={() => {
                this.onOrOff(true);
              }}
            />
            <Icon
              type="font-awesome"
              name="unlock"
              color="#009788"
              reverse
              raised
              component={TouchableOpacity}
              size={30}
              style={{ fontSize: 30 }}
              onPress={() => {
                this.onOrOff(false);
              }}
            />

          </View>



        </View>
        <View style={styles.footer}>


          {/* <Switch
            style={{ bottom: 13, left: 28 }}
            tintColor="#727272"
            onTintColor="#fff"
            value={this.state.isLocal}
            onValueChange={isOn => {
              this.setState({ isLocal: isOn });
            }}
          /> */}

          <TouchableWithoutFeedback
            
            onPress={() => {

              this.setState({ isLocal: !this.state.isLocal });

              if (this.state.isLocal)
                this.animation.play(1, 29);
              else
                this.animation.play(29, 59);
            }}
          >
            <LottieView
              style={{ width: 100, height: 50,bottom: 13, left: 25 }}
              ref={animation => {
                this.animation = animation;
              }}
              source={require('./assets/lottie/switch.json')}
              loop={false}
            />
          </TouchableWithoutFeedback>

          <Text style={styles.footerLabel}>
            {this.state.isLocal ? "شبکه داخلی" : "اینترنت"}
          </Text>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
    flex: 1
  },
  contentContainer: {
    flex: 4,
    justifyContent: "center",
    alignItems: "center"
  },
  headerTitle: {
    textAlign: "center",
    color: "#fff",
    margin: 15,
    fontSize: 30
  },
  label: {
    textAlign: "center",
    color: "#fff",
    margin: 15,
    fontSize: 25
  },
  footerLabel: {
    textAlign: "center",
    color: "#fff",
    marginHorizontal: 15,
    bottom: 10,
    paddingTop: 5,
    fontSize: 15,
    width: 100
  },
  footer: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "flex-end"
  },
  actionButtons: {
    margin: 25
  }
});
